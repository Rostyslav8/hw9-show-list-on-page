// Завдання

// Реалізувати функцію, яка отримуватиме масив елементів і виводити їх на сторінку у вигляді списку.Завдання має бути виконане на чистому Javascript без використання бібліотек типу jQuery або React.

// Технічні вимоги:

// Створити функцію, яка прийматиме на вхід масив і опціональний другий аргумент parent - DOM - елемент, до якого буде прикріплений список(по дефолту має бути document.body.)
// кожен із елементів масиву вивести на сторінку у вигляді пункту списку;
// Приклади масивів, які можна виводити на екран:

// ["hello", "world", "Kiev", "Kharkiv", "Odessa", "Lviv"];
// ["1", "2", "3", "sea", "user", 23];
// Можна взяти будь - який інший масив.

function elementsList(array, parentUl = document.body) {
  let ul = document.createElement("ul");
  array.forEach((element) => {
    let li = document.createElement("li");
    li.append(element);
    ul.append(li);
  });
  parentUl.prepend(ul);
}

elementsList(["hello", "world", "Kiev", "Kharkiv", "Odessa", "Lviv"]);




